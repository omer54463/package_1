from package_1.package_1 import Package1


def main() -> None:
    package_1 = Package1()
    package_1.method()


if __name__ == "__main__":
    main()
